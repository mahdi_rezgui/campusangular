import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CampusFormComponent } from './components/campus-form/campus-form.component';
import { ListCampusComponent } from './components/list-campus/list-campus.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'campus', component: CampusFormComponent },
  { path: 'campusList', component: ListCampusComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
