import { Component, OnInit } from '@angular/core';
import { Campus } from 'src/app/models/campus';
import { CampusService } from 'src/app/services/campus.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-campus-form',
  templateUrl: './campus-form.component.html',
  styleUrls: ['./campus-form.component.css']
})
export class CampusFormComponent implements OnInit {

  currentCampus = {
    id: 1,
    name: "Campus Manar",
    location: "El Manar",
    dateOfCreation: null
  }
  // currentCampus: Campus;
  //name : string = "Campus";
  isEdit: boolean = false;
  constructor(private campusService: CampusService, private modalService: NgbModal) { }

  ngOnInit() {
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  // addCampus({ value, valid }: { value: Campus, valid: boolean }) {

  //   if (!valid) {
  //     console.log('Form is not Valid !')
  //   } else {

  //     console.log(value)
  //   }
  // }

  addCampus(value: Campus) {
    // console.log(value)
    this.campusService.addCampusToList(value).subscribe(data => {
      console.log(data);
    },
      err => {
        console.log(err);
      });

  }
  update() {

  }
}


