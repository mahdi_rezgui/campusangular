import { Component, OnInit } from '@angular/core';
import { Campus } from 'src/app/models/campus';
import { CampusService } from 'src/app/services/campus.service';

@Component({
  selector: 'app-list-campus',
  templateUrl: './list-campus.component.html',
  styleUrls: ['./list-campus.component.css']
})
export class ListCampusComponent implements OnInit {

  listCampus: Campus[];

  constructor(private campusService: CampusService) { }

  ngOnInit() {

    this.campusService.getCampusList().subscribe(list => {
      this.listCampus = list;
    });
    // this.listCampus =[{
    //   id: 1,
    //   name: "Campus Manar",
    //   location: "El Manar",
    //   dateOfCreation: null
    // },{
    //   id: 2,
    //   name: "Campus Manar",
    //   location: "El Manar",
    //   dateOfCreation: null
    // },{
    //   id: 3,
    //   name: "Campus Manar",
    //   location: "El Manar",
    //   dateOfCreation: null
    // }] ;
  }

}
