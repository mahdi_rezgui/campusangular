import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Campus } from '../models/campus';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  //  'Access-Control-Allow-Origin': '*'
  })
}


@Injectable({
  providedIn: 'root'
})
export class CampusService {

  campusUrl: string = 'http://localhost:8080/getAllCampus';
  campusPostUrl: string = 'http://localhost:8080/createCampus'

  constructor(private http: HttpClient) { }

  getCampusList(): Observable<Campus[]> {
    // let headers = new HttpHeaders();
    // headers.append('Access-Control-Allow-Origin', '*');

    return this.http.get<Campus[]>(this.campusUrl, httpOptions);
  }

  addCampusToList(campus: Campus): Observable<Campus>{
    return this.http.post<Campus>(this.campusPostUrl, campus);
  }
}
